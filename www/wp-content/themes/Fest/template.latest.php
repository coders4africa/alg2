<?php
/*
Template Name: Latest Posts
*/

global $tpl;

gk_load('header');
gk_load('before');

global $more;
$more = 0;

// get the page number
$paged = (get_query_var('paged')) ? get_query_var('paged') : ((get_query_var('page')) ? get_query_var('page') : 1);

query_posts('posts_per_page=' . get_option('posts_per_page') . '&paged=' . $paged );

?>


<div id="info">
<p> 
<strong>For Media Interviews:</strong> For more information, or to schedule a phone interview with PLAD representatives or the performing artists, 
<br>Please Contact: <strong class="staff">LGB Productions<strong>,    <em class="email">marketing@lgbproductions.com</em> 
<br> <strong>#africanlandgrab</strong>

</p>

<p>
<h2>Video of Forum on Remittances,<br> March 29, 2014 -Washington, D.C.(English version)</h2>

<iframe width="90%" height="400"  src="//www.youtube.com/embed/8h3PF2WyNiE" frameborder="0" allowfullscreen></iframe>

<br>

<h2>VPLAD FORUM: Fonds des émigrés, <br> M29 Mars 2014 (French version)</h2>

<iframe width="90%" height="400"  src="//www.youtube.com/embed/Tm-w06Pa3xA" frameborder="0" allowfullscreen></iframe>


</p>

</div>
<?php if ( have_posts() ) : ?>
	<section id="gk-mainbody">	
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; ?>
		
		<?php gk_content_nav(); ?>
		
		<?php wp_reset_query(); ?>
	</section>
<?php else : ?>
	<section id="gk-mainbody">
		<article id="post-0" class="post no-results not-found">
			<header class="entry-header">
				<h1 class="entry-title"><?php _e( 'Nothing Found', GKTPLNAME ); ?></h1>
			</header>

			<div class="entry-content">
				<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', GKTPLNAME ); ?></p>
				<?php get_search_form(); ?>
			</div>
		</article>
	</section>
<?php endif; ?>

<?php

gk_load('after');
gk_load('footer');

// EOF